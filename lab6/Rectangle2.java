public class Rectangle2 {

    private Point topLeft;
    private int width;
    private int height;

    public Rectangle2(Point topLeft, int width, int height) {
        this.topLeft = topLeft;
        this.width = width;
        this.height = height;
    }

    public int area() {
        return width * height;
    }

    public int perimenter() {
        return 2 * (width + height);
    }

    public Point[] corners() {
        Point[] corners = new Point[4];
        corners[0] = topLeft;
        corners[1] = new Point(topLeft.getxCoord() + width, topLeft.getyCoord());
        corners[2] = new Point(topLeft.getxCoord(), topLeft.getyCoord() - height);
        corners[3] = new Point(topLeft.getxCoord() + width, topLeft.getyCoord() - height);
        return corners;
    }
}
