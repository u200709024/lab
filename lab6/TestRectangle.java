public class TestRectangle {
    public static void main(String[] args){


        Rectangle2 r = new Rectangle2(new Point(1,2),10,12);

        System.out.println("Area of rectangle is "+r.area());

        System.out.println("Perimeter of rectangle is "+r.perimenter());

        Point[] corners = r.corners();

        for (int i = 0; i < corners.length; i++) {
            Point p = corners[i];
            if (p == null) {
                System.out.println("p is null");
            }
            else {
                System.out.println("Corner " + (i+1) + " x = " + p.getxCoord()+ " y = " + p.getyCoord());
            }
        }
    }
}
