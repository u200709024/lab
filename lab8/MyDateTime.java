public class MyDateTime extends Object {
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime myTime) {
        super();
        this.date = date;
        this.time = myTime;
    }

    public String toString() {
        return date + " " + time;
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        incrementHour(1);
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incrementHour(diff);
        if (dayDiff < 0) {
            date.decrementDay(-dayDiff);
        } else {
            date.incrementDay(dayDiff);
        }
    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {
        int dayDiff = time.incrementMinute(diff);
        if (dayDiff < 0) {
            date.decrementDay(-dayDiff);
        } else {
            date.incrementDay(dayDiff);
        }
    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int i) {
        date.incrementDay(i);
    }

    public void decrementMonth(int i) {
        date.decrementMonth(i);
    }

    public void decrementDay(int i) {
        date.decrementDay(i);
    }

    public void incrementMonth(int i) {
        date.incrementMonth(i);
    }

    public void decrementYear(int i) {
        date.decrementYear(i);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        boolean isBeforeForHour = false;
        if (date.year == anotherDateTime.date.year && date.month == anotherDateTime.date.month && date.day == anotherDateTime.date.day) {
            if (time.hour < anotherDateTime.time.hour) {
                isBeforeForHour = true;
            } else if (time.hour == anotherDateTime.time.hour && time.minute < anotherDateTime.time.minute) {
                isBeforeForHour = true;
            }
        }
        return isBeforeForHour;
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        boolean isAfterForHour = true;
        if (date.year == anotherDateTime.date.year && date.month == anotherDateTime.date.month && date.day == anotherDateTime.date.day) {
            if (time.hour < anotherDateTime.time.hour) {
                isAfterForHour = false;
            } else if (time.hour == anotherDateTime.time.hour && time.minute < anotherDateTime.time.minute) {
                isAfterForHour = false;
            }
        }
        return isAfterForHour;
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
        String dayTimeDiff = null;
        String hourDiff  = null;
        String minuteDiff = null;
        String yearDiff = null;
        String monthDiff = null;
        String dayDiff = null;
        if (date.year == anotherDateTime.date.year && date.month == anotherDateTime.date.month && date.day == anotherDateTime.date.day) {
                hourDiff = String.valueOf(anotherDateTime.time.hour - time.hour);
                minuteDiff = String.valueOf(anotherDateTime.time.minute - time.minute);
                yearDiff = String.valueOf(0);
                monthDiff = String.valueOf(0);
                dayDiff = String.valueOf(0);
            }
        else{
            if (anotherDateTime.date.day - date.day <0 || anotherDateTime.date.month - date.month < 0){
                date.month++;
                anotherDateTime.date.day+=30;
                anotherDateTime.time.hour += 23;
                anotherDateTime.time.minute +=60;
            }
            yearDiff = String.valueOf(anotherDateTime.date.year- date.year);
            monthDiff = String.valueOf(anotherDateTime.date.month - date.month);
            dayDiff = String.valueOf(anotherDateTime.date.day - date.day);
            hourDiff = String.valueOf(anotherDateTime.time.hour - time.hour);
            minuteDiff = String.valueOf(anotherDateTime.time.minute - time.minute);
        }
        return yearDiff + " year(s) " + monthDiff + " month(s) " + dayDiff + " day(s) " + hourDiff +" hour(s) " + minuteDiff + " minute(s)";
        }
    }

