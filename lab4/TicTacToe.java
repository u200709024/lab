import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
        int moveCount = 0;
        printBoard(board);
        int currentPlayer = 0;
        while (moveCount < 9) {

            System.out.print("Player" + (currentPlayer + 1) + " enter row number:");
            int row = reader.nextInt();
            System.out.print("Player" + (currentPlayer + 1) + " enter column number:");
            int col = reader.nextInt();
            if (row > 0 && row < 4 && col > 0 && col < 4 && board[row - 1][col - 1] == ' ') {


                if (currentPlayer == 0) {
                    board[row - 1][col - 1] = 'X';
                } else {
                    board[row - 1][col - 1] = 'O';
                }
                // şu şekilde de yapılabilir board[row - 1][col - 1] = currentPlayer == 0 ? "X" : "O";
                currentPlayer = (currentPlayer + 1) % 2;
                moveCount++;

                printBoard(board);
                boolean win = checkboard(board, row - 1, col - 1);
                if (win) {
                    System.out.println("Player " + (currentPlayer + 1 ) + " is the winner");
                    break;
                }


            } else {
                System.out.println("It's not a valid move");
            }
        }


        reader.close();
    }


    private static boolean checkboard(char[][] board, int row, int col) {
        char symbol = board[row][col];
        boolean win = true;
        for (int c = 0; c < 3; c++) {
            if ((board[row][c]) != symbol) {
                win = false;
                break;
            }
        }
        if (win)
            return true;
        win = true;
        for (int r = 0; r < 3; r++) {
            if ((board[r][col]) != symbol) {
                win = false;
                break;
            }
        }
        if (win)
            return true;

        if (row == col) {
            win = true;
            for (int i = 0; i < 3; i++) {
                if ((board[i][i]) != symbol) {
                    win = false;
                    break;

                }
            }
        }
        if (win)
            return true;

        win = true;
        for (int i = 0,  j = 2; j > -1 &&  i < 3; --j, i++) {
            if ((board[j][i]) != symbol) {
                win = false;
                break;

            }
        }

        if (win)
            return true;



        return false;
    }


    public static void printBoard(char[][] board){
        System.out.println("    1   2   3");
        System.out.println("   -----------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");
            }
            System.out.println();
            System.out.println("   -----------");
        }

    }

}