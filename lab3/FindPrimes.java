public class FindPrimes {
    public static void main(String[] args) {
        int max = Integer.parseInt(args[0]);
        // print the max less than max
        // For each number less than max
        for (int number = 2; number < max; number++) {
            // check if the number prime or not
            // let divisor = 2
            int divisior = 2;
            // let isPrime = true;
            boolean isPrime = true;
            // while divisor less than number
            while (divisior < number && isPrime) {
                // if the number is divisible by divisor
                if (number % divisior == 0)
                    isPrime = false;
                //isPrime = false;
                // increment divisor
                divisior++;
            }
            // if the number is prime
            if (isPrime)
                System.out.println(number + " ");
            // print the number
        }
    }
}